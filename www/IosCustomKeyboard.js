var exec = require('cordova/exec');

//keyboard pattern;
// 1 =  UIKeyboardTypeDefault;              
// 2 =  UIKeyboardTypeASCIICapable;       
// 3 = UIKeyboardTypeNumbersAndPunctuation;
// 4 =  UIKeyboardTypeURL;                 
// 5 =  UIKeyboardTypeNumberPad;      
// 6 =  UIKeyboardTypePhonePad;  
// 7 =  UIKeyboardTypeNamePhonePad;
// 8 =  UIKeyboardTypeEmailAddress; 
// 9 =  UIKeyboardTypeDecimalPad;
// 10 =  UIKeyboardTypeTwitter;
// 11 =  UIKeyboardTypeWebSearch;

module.exports.open = function(value, keyboard, onChange, onFinished) {
    !keyboard && (keyboard = 1);
    !value && (value = '');
    value = '' + value;
    exec(onChange, onFinished, "IosCustomKeyboard", "open", [value, keyboard]);
};

module.exports.change = function(value) {
    !value && (value = '');
    value = '' + value;
    exec(null, null, "IosCustomKeyboard", "change", [value]);
};

module.exports.close = function(callback) {
    exec(callback, null, "IosCustomKeyboard", "close", []);
};